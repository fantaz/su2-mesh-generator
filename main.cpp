//
// This file is part of su2MeshGenerator <https://bitbucket.org/fantaz/su2-mesh-generator>
// You can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// $URL$
// $Id$
//
//
// Author(s)     : Jerko Skific <skific@riteh.hr>

#include <fstream>
#include "io_yaml.h"

#include "muParser.h"

double MySqr(double a_fVal)
{
    return a_fVal*a_fVal;
}

void test_muParser()
{
    /*
    double fVal = 1;
    mu::Parser p;
    p.DefineVar("a", &fVal);
    p.DefineFun("MySqr", MySqr);
    p.SetExpr("MySqr(a)*_pi+min(10,a)");

    for (std::size_t a=0; a<100; ++a)
    {
        fVal = a;  // Change value of variable a
        std::cout << p.Eval() << std::endl;
    }*/
}

void defaultFunctions()
{
    su2mg::Project prj;
    prj.write_function_example("write_function_example.yaml");
    prj.readInput("write_function_example.yaml");
    prj.generateMesh();
    prj.generateOutput("write_function_example");
    /*
    su2mg::Project prj;
    prj.write_example("example_two_holes.yaml");
    prj.readInput("example_two_holes.yaml");
    prj.generateMesh();
    prj.generateOutput("example_two_holes");
    
    /*
    prj.write_nozzle_in_res("nozzle_in_res.yaml");
    prj.readInput("nozzle_in_res.yaml");
    prj.generateMeh();
    prj.generateOutput("nozzle_in_res");
    */

    /*prj.write_sod_unsteady("sod_unstedy.yaml");
    prj.readInput("sod_unstedy.yaml");
    prj.generateMesh();
    prj.generateOutput("sod_unstedy");
    */
    /*
    prj.write_problem_7_8_a_steady("problem_7_8_a_steady.yaml");
    prj.readInput("problem_7_8_a_steady.yaml");
    prj.generateMeh();
    prj.generateOutput("problem_7_8_a_steady");
    */
    /*prj.write_nozzle_only("new_nozzle_only.yaml");
    prj.readInput("new_nozzle_only.yaml");
    prj.generateMeh();
    prj.generateOutput("new_nozzle_only");
    */

    /*prj.write_nozzle_in_freestream("nozzle_in_free_stream.yaml");
    prj.readInput("nozzle_in_free_stream.yaml");
    prj.generateMeh();
    prj.generateOutput("nozzle_in_free_stream");*/

    /*prj.write_example("blunt_1.yaml");
    prj.write_nozzle_in_res("ex2.yaml");
    prj.write_nozzle_only("ex3.yaml");

    prj.readInput("ex3.yaml");
    prj.generateMeh();
    prj.generateOutput("ex3_out");*/
}

void generateMesh(const std::string& i, const std::string& o)

{
    su2mg::Project prj;
    prj.readInput(i);
    prj.generateMesh();
    prj.generateOutput(o);
}
static void show_usage(std::string name)
{
    std::cerr << "Usage: " << name << " <option(s)> SOURCES \n"
              << "Options:\n"
              << "\t-h,--help\t\tShow this help message\n"
              << "\t-i,--input INPUT\tSpecify the input yaml file\n"
              << "\t-o,--output OUTPUT\tSpecify the output base file for vtu and su2 file"
              << std::endl;
}

int main(int argc, char* argv[])
{   
    if (argc < 2) {
        show_usage(argv[0]);
        std::cout<<"Instead, I will go with default: "<<std::endl;
        defaultFunctions();
        return 0;
    }
    std::vector <std::string> sources;
    std::string input;
    std::string output;
    for (int i = 1; i < argc; ++i)
    {
        std::string arg = argv[i];
        if ((arg == "-h") || (arg == "--help"))
        {
            show_usage(argv[0]);
            return 0;
        }
        else if ((arg == "-i") || (arg == "--input"))
        {
            if (i + 1 < argc) // Make sure we aren't at the end of argv!
            {
                input = argv[++i]; // Increment 'i' so we don't get the argument as the next argv[i].
            }
            else  // there was no argument to the input option.
            {
                std::cerr << "--input option requires one argument." << std::endl;
                return 1;
            }
        }
        else if ((arg == "-o") || (arg == "--output"))
        {
            if (i + 1 < argc) // Make sure we aren't at the end of argv!
            {
                output = argv[++i]; // Increment 'i' so we don't get the argument as the next argv[i].
            }
            else
            {   // Uh-oh, there was no argument to the destination option.
                std::cerr << "--output option requires one argument." << std::endl;
                return 1;
            }
        }
        else
        {
            sources.push_back(argv[i]);
        }
    }
    std::cout<<"Input file "<<input<<std::endl;
    std::cout<<"Output files " <<output+".vtu"<< " and "<<output+".su2"<<std::endl;
    generateMesh(input,output);
    return 0;
}
