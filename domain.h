//
// This file is part of su2MeshGenerator <https://bitbucket.org/fantaz/su2-mesh-generator>
// You can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// $URL$
// $Id$
//
//
// Author(s)     : Jerko Skific <skific@riteh.hr>
#ifndef _DOMAIN_H_
#define _DOMAIN_H_
#include <string>
#include <vector>
#include <algorithm>
#include <limits>
#include "yaml-cpp/yaml.h"
#include "boundary.h"
#include "interpolation.h"
namespace su2mg {
template<typename cdtPoint, typename Vertex_handle>class Domain
{
private:
    //! \brief domain name
    std::string name;

    //! \brief domain point list as defined in yaml file
    std::vector<cdtPoint> poly;

    //! \brief list of boundaries
    std::vector<Boundary> boundaries;

    //! \brief list of interpolations
    std::vector<Interpolation<cdtPoint>> interpolations;
public:
    //! \brief interpolate
    void applyInterpolations()
    {
        // nema interpolacija, nemamo nista za raditi
        if(interpolations.empty()) {
            return;
        }
        // samo test
        for(auto& interp: interpolations)
        {
            std::vector<cdtPoint> o_pts;
            for(const auto& id : interp.getPtIds())
            {
                o_pts.push_back(poly[id]);
            }
            interp.generatePoints(o_pts);
        }
        // sada kad sam interpolirao tocke, vrijeme je da ih ubacim:
        // ne mogu ovo raditi gore jer ce mi zeznuti poly...
        std::vector<cdtPoint> new_poly;

        new_poly = poly;
        std::map<unsigned,unsigned> old_new_ids;
        unsigned cumulative_num_of_inserted_points=0;

        // sortiram tako da prvi u vektoru bude sa najmanjim ptdids
        // dode do greske ako se zamijene indexi sa suprotnom stranom
        std::sort(interpolations.begin(),interpolations.end(), [](Interpolation<cdtPoint> a, Interpolation<cdtPoint> b) {
            return a.getPtIds()[0]<b.getPtIds()[0];
        });


        for(auto& interp: interpolations)
        {

            auto it = new_poly.begin();
            std::vector<cdtPoint> vec = interp.getInterpPoints();
            unsigned old_pos = interp.getPtIds()[1] + cumulative_num_of_inserted_points;

            // jer zadnji moze spajati tocku s indexom 3 i tocku s indexom 0
            if(interp.getPtIds()[0]<interp.getPtIds()[1])
            {
                new_poly.insert(it+old_pos,vec.begin(), vec.end());
            }
            else
            {
                new_poly.insert(new_poly.end(),vec.begin(), vec.end());
            }

            old_new_ids.insert(std::make_pair(interp.getPtIds()[0],
                                              interp.getPtIds()[0]+cumulative_num_of_inserted_points));
            cumulative_num_of_inserted_points+=interp.numberOfInterpolatedPoints();
            if(interp.getPtIds()[0]<interp.getPtIds()[1])
            {   old_new_ids.insert(std::make_pair(interp.getPtIds()[1],
                                                  interp.getPtIds()[1]+cumulative_num_of_inserted_points));
            }
            /*else{old_new_ids.insert(std::make_pair(std::numeric_limits<unsigned>::max(),
                interp.getPtIds()[0]+cumulative_num_of_inserted_points));}*/
        }

        std::cout<<"start old_new ids"<<std::endl;
        for(const auto& p : old_new_ids)
        {
            std::cout<<p.first<<" , "<<p.second<<std::endl;
        }
        std::cout<<"end old_new ids"<<std::endl;

        unsigned poly_pt_num(0);
        std::vector<unsigned> poly_indices(poly.size());
        std::generate(poly_indices.begin(), poly_indices.end(), [&] { return poly_pt_num++; });

        for(const auto& id : poly_indices)
        {
            std::cout<<"id ->"<<id<<std::endl;
        }
        std::map<unsigned,unsigned> old_to_new_ids;
        for(int i=0; i<poly_indices.size(); ++i)
        {
            auto it = old_new_ids.find(poly_indices[i]);
            if (it != old_new_ids.end())
            {
                old_to_new_ids.insert(std::make_pair(poly_indices[i],it->second));
            }
            else if (i==0) // ako nismo pokupili prvu tocku
            {
                old_to_new_ids.insert(std::make_pair(poly_indices[i],it->second));
            }
            else if(i>0) // prva tocka je pokupljena
            {
                auto it2 = old_to_new_ids.find(poly_indices[i-1]);
                if(it2 != old_to_new_ids.end())
                {
                    old_to_new_ids.insert(std::make_pair(poly_indices[i],it2->second+1));
                }
            }
        }
        std::cout<<"start old_to_new_ids"<<std::endl;
        for(const auto& p : old_to_new_ids)
        {
            std::cout<<p.first<<" , "<<p.second<<std::endl;
        }
        std::cout<<"end old_to_new_ids"<<std::endl;
        poly = new_poly;

        // sada idemo nastimati rubove
        for(auto& bnd : boundaries)
        {
            std::vector<unsigned> new_ptd_ids;
            //std::cout<<"start bnd ids"<<std::endl;
            for(const auto& ptid: bnd.ptids)
            {
                // std::cout<<ptid<<std::endl;
                auto it = old_to_new_ids.find(ptid);
                if (it != old_to_new_ids.end()) {
                    new_ptd_ids.push_back(it->second);
                }
                else {
                    new_ptd_ids.push_back(ptid);
                }
            }
            //std::cout<<"end bnd ids"<<std::endl;
            /*
            std::cout<<"start new_ptd_ids"<<std::endl;
            for(const auto& ptid : new_ptd_ids)
            { std::cout<<ptid<<std::endl; }
            std::cout<<"end new_ptd_ids"<<std::endl;*/
            std::vector<unsigned> ranges;
            ranges = new_ptd_ids;
            for(int i=0; i<ranges.size()-1; ++i)
            {
                int delta = (int)ranges[i+1]-(int)ranges[i];
                assert(delta != 0);
                if(delta>1)
                {
                    std::vector<unsigned> tmp;
                    tmp.resize(delta-1);
                    unsigned n(ranges[i]+1);
                    std::generate(tmp.begin(), tmp.end(), [&] { return n++; });
                    auto res = std::find(new_ptd_ids.begin(), new_ptd_ids.end(), ranges[i]);
                    new_ptd_ids.insert(res+1,tmp.begin(), tmp.end());
                }
                if(delta<0)
                {
                    std::vector<unsigned> tmp;
                    tmp.resize(new_poly.size()-ranges[i]-1);
                    unsigned n(ranges[i]+1);
                    std::generate(tmp.begin(), tmp.end(), [&] { return n++; });
                    auto res = std::find(new_ptd_ids.begin(), new_ptd_ids.end(), ranges[i]);
                    new_ptd_ids.insert(res+1,tmp.begin(), tmp.end());
                }
            }
            std::cout<<"start generated new_ptd_ids"<<std::endl;
            for(const auto& ptid : new_ptd_ids)
            {
                std::cout<<ptid<<std::endl;
            }
            std::cout<<"end generated new_ptd_ids"<<std::endl;
            bnd.ptids = new_ptd_ids;
        }
    }

    //! \brief exposes point list
    std::vector<cdtPoint>& getPoly() {
        return poly;
    }

    //! \brief exposes boundary list
    std::vector<Boundary>& getBoundaries() {
        return boundaries;
    }

    //! \brief gets number of boundaries
    size_t numberOfBoundaries() const {
        return boundaries.size();
    }

    //! \brief list of vertices based on domain point list (\see poly) that are part of triangulation.
    //! The map consists of id (usigned) and vertex handle that is part of triangulation
    std::map<unsigned, Vertex_handle> initial_boundary_vertices;

    //! \brief same thing as initial_boundary_vertices, but the key is Vertex_handle
    std::map<Vertex_handle, unsigned> vertices_ids;

    //! \brief checks if vertices with ids ptida and ptidb are on edge defined with va and vb
    //! true if boundary was found, false otherwise
    bool insertSubConstraint(Vertex_handle va, Vertex_handle vb,unsigned ptida, unsigned ptidb)
    {
        auto ita = vertices_ids.find(va);
        auto itb = vertices_ids.find(vb);
        if(ita != vertices_ids.end() && itb != vertices_ids.end())
        {
            for(auto& bnd : boundaries)
            {
                if(bnd.isEdgeOnBounary(ita->second, itb->second))
                {
                    bnd.mesh_boundary_ids.push_back(std::make_tuple(ptida,ptidb));
                }
            }
            return true;
        }
        return false;
    }

    void fromYamlNode(YAML::Node n)
    {
        if(n["name"])
        {
            name = n["name"].as<std::string>();
        }
        if(n["points"])
        {
            for(const auto &ptnode : n["points"])
            {
                double x = ptnode[0].as<double>();
                double y = ptnode[1].as<double>();
                poly.push_back(cdtPoint(x,y));
            }
        }
        if(n["interpolations"])
        {
            for(const auto &interp_node : n["interpolations"])
            {
                Interpolation<cdtPoint> interp;
                interp.fromYamlNode(interp_node);
                interpolations.push_back(interp);
            }
        }
        if(n["boundaries"])
        {
            for(const auto &bndnode : n["boundaries"])
            {
                Boundary bnd;
                bnd.fromYamlNode(bndnode);
                boundaries.push_back(bnd);
            }
        }
    }
    YAML::Node toYamlNode() const
    {
        YAML::Node nd;
        nd["name"] = name;
        YAML::Node p;
        for(const cdtPoint& pt: poly)
        {
            p.push_back(pt.x());
            p.push_back(pt.y());
        }
        nd["points"] = p;
        YAML::Node b;
        for(const Boundary& bnd: boundaries)
        {
            b.push_back(bnd.toYamlNode());
        }
        nd["boundaries"] = b;
        return nd;
    }
};
}
#endif //_LINEAR_SPLINE_H_


