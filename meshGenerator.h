//
// This file is part of su2MeshGenerator <https://bitbucket.org/fantaz/su2-mesh-generator>
// You can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// $URL$
// $Id$
//
//
// Author(s)     : Jerko Skific <skific@riteh.hr>

#ifndef _MESHGENERATOR_H_
#define _MESHGENERATOR_H_
#include <CGAL/Lipschitz_sizing_field_2.h>
#include <CGAL/Lipschitz_sizing_field_criteria_2.h>
#include <CGAL/Delaunay_mesher_2.h>


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include <CGAL/Polygon_2.h>


#include <time.h>


#include <list>
namespace su2mg {
//! \brief meshing superclass
template <typename Triangulation>
class MeshGeneratorType
{
    typedef typename Triangulation::Point Point;
    typedef typename std::list<Point>::iterator iter;
public:
    MeshGeneratorType(Triangulation& _c) : cdt(_c) {}
    virtual std::string type() const = 0;
    virtual YAML::Node toYamlNode() = 0;
    virtual void generateMesh(iter bit, iter eit)=0;
protected:
    Triangulation &cdt;
};

//! \brief simple meshing
template<typename Triangulation, typename Critera>
class SimpleMeshGenerator : public MeshGeneratorType<Triangulation>
{
    typedef typename Triangulation::Point Point;
    typedef typename std::list<Point>::iterator iter;
    typedef typename CGAL::Delaunay_mesher_2<Triangulation, Critera> Mesher;
public:
    SimpleMeshGenerator(Triangulation& _c, double ms, double ma):
        MeshGeneratorType<Triangulation>(_c), meshSize(ms), meshAngle(ma) {}
    std::string type() const {
        return "Simple";
    }
    YAML::Node toYamlNode()
    {
        YAML::Node nd;
        nd["type"] = type();
        nd["TriangleSize"] = meshSize;
        nd["TriangleAngle"] = meshAngle;
        return nd;
    }
    void generateMesh(iter bit, iter eit)
    {
        Mesher mesher(this->cdt);
        mesher.set_seeds(bit, eit);
        mesher.refine_mesh();
        mesher.set_criteria(Critera(meshAngle,meshSize));
        mesher.refine_mesh();
    }
private:
    double meshSize;
    double meshAngle;
};

//! \brief Lipschitz mesher
template<typename Kernel,typename Triangulation, typename Critera >
class LipschitzMeshGenerator : public MeshGeneratorType<Triangulation>
{
    typedef typename Triangulation::Point Point;
    typedef typename std::list<Point>::iterator iter;
    typedef typename Triangulation::Finite_edges_iterator Finite_edges_iterator;
    typedef typename Triangulation::Segment Segment;
    typedef typename Kernel::Point_2 Point_2;
public:
    LipschitzMeshGenerator(Triangulation& _c, double _k, double ma):
        MeshGeneratorType<Triangulation>(_c), k(_k), meshAngle(ma) {}
    std::string type() const {
        return "Lipschitz";
    }
    YAML::Node toYamlNode()
    {
        YAML::Node nd;
        nd["type"] = type();
        nd["k"] = k;
        nd["TriangleAngle"] = meshAngle;
        return nd;
    }
    void generateMesh(iter bit, iter eit)
    {
        CGAL::refine_Delaunay_mesh_2(this->cdt, bit, eit,Critera());
        //doGenerateMesh(bit, eit);
    }
private:
    void  doGenerateMesh(iter bit, iter eit)
    {
        std::set<Point_2> points;
        for (Finite_edges_iterator eit = this->cdt.finite_edges_begin();
                eit != this->cdt.finite_edges_end(); ++eit)
        {
            if (this->cdt.is_constrained(*eit))
            {
                Segment s = this->cdt.segment(*eit);
                std::cout << "" << s.source() << " " << s.target() << std::endl;
                points.insert(s.source());
                points.insert(s.target());
            }
        }
        std::cout << "Points start" << std::endl;
        for(auto& p : points)
        {
            std::cout << "" << p.x() << " " << p.y() << std::endl;
        }


        typedef typename CGAL::Lipschitz_sizing_field_2<Kernel> Lipschitz_sizing_field;
        typedef typename CGAL::Lipschitz_sizing_field_criteria_2<Triangulation, Lipschitz_sizing_field> Lipschitz_criteria;
        typedef typename CGAL::Delaunay_mesher_2<Triangulation, Lipschitz_criteria> Lipschitz_mesher;

        Lipschitz_sizing_field field(points.begin(), points.end(), k ); // k-lipschitz with k=1
        Lipschitz_criteria criteria(meshAngle, &field);
        Lipschitz_mesher mesher(this->cdt);

        mesher.set_criteria(criteria);
        mesher.init(true);
        mesher.set_seeds(bit, eit);
        mesher.clear_seeds();
        mesher.refine_mesh();

        std::set<Point_2> isolated_domain;
        Segment s1(Point_2(20,10), Point_2(-10,10));
        Segment s2(Point_2(-10,10), Point_2(-10,5));
        Segment s3(Point_2(-10,5), Point_2(20,5));
        Segment s4(Point_2(20,5), Point_2(20,10));

        isolated_domain.insert(s1.source());
        isolated_domain.insert(s1.target());
        isolated_domain.insert(s2.source());
        isolated_domain.insert(s2.target());
        isolated_domain.insert(s3.source());
        isolated_domain.insert(s3.target());
        isolated_domain.insert(s4.source());
        isolated_domain.insert(s4.target());

        double max_x = 20, min_x = -10;
        double max_y = 10 , min_y = 5;
        srand(time(NULL));
        for(int i=0; i<100; i++)
        {
            double x = min_x + double(( (max_x-min_x) * rand()) / (RAND_MAX + 1.0));
            double y = min_y + double(( (max_y-min_y) * rand()) / (RAND_MAX + 1.0));
            std::cout << "Ubacujem " << x << ", " << y << std::endl;
            isolated_domain.insert(Point_2(x,y));
        }
        k = k;
        meshAngle = meshAngle;
        Lipschitz_sizing_field isolated_domain_field(isolated_domain.begin(), isolated_domain.end(), k );
        Lipschitz_criteria isolated_domain_field_criteria(meshAngle, &isolated_domain_field);
        mesher.set_criteria(isolated_domain_field_criteria);
        mesher.refine_mesh();
    }
private:
    double k;
    double meshAngle;
};
} // end namespace su2mg
#endif //_MESHGENERATOR_H_
