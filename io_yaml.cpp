//
// This file is part of su2MeshGenerator <https://bitbucket.org/fantaz/su2-mesh-generator>
// You can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// $URL$
// $Id$
//
//
// Author(s)     : Jerko Skific <skific@riteh.hr>
#include "io_yaml.h"
#include <algorithm>
#include <valarray>
#include <cmath>
#include <limits>
#include "muParser.h"
#include <CGAL/squared_distance_2.h>
#include <string>
#include "interpolation.h"
#include "pathconfig.h"

#include <CGAL/Delaunay_mesh_area_criteria_2.h>
namespace su2mg {
/////////////////////////////////////////////////////////////
void Project::fromYamlNode(YAML::Node n)
{
    if(n["meshDimension"])
    {
        meshDim = n["meshDimension"].as<int>();
    }
    if(n["mesh"])
    {
        //meshSize = n["mesh"]["TriangleSize"].as<double>();
        //meshAngle = n["mesh"]["TriangleAngle"].as<double>();
        if(n["mesh"]["type"].as<std::string>()=="Simple")
        {
            double mS = n["mesh"]["TriangleSize"].as<double>();
            double mA = n["mesh"]["TriangleAngle"].as<double>();
            mesh_generator = std::make_shared<SimpleMeshGenerator<Triangulation,CritPlus>>(cdt,mS,mA);
        }
        else if(n["mesh"]["type"].as<std::string>()=="Lipschitz")
        {
            double k = n["mesh"]["k"].as<double>();
            double mA = n["mesh"]["TriangleAngle"].as<double>();
            mesh_generator = std::make_shared<LipschitzMeshGenerator<K,Triangulation,CritPlus>>(cdt,k,mA);
        }
    }
    if(n["domain"])
    {
        domain.fromYamlNode(n["domain"]);
    }
    if(n["holes"])
    {
        for(const auto &holenode : n["holes"])
        {
            Hole<cdtPoint,Vertex_handle> h;
            h.fromYamlNode(holenode);
            holes.push_back(h);
        }
    }
    if(n["initial conditions"])
    {
        for(const auto &icnode : n["initial conditions"])
        {
            InitialConditions<cdtPoint> ic;
            ic.fromYamlNode(icnode);
            ic_list.push_back(ic);
        }
    }
}

/////////////////////////////////////////////////////////////
YAML::Node Project::toYamlNode() const
{
    YAML::Node nd;
    nd["MeshDimension"] = meshDim;

    if(mesh_generator)
    {
        nd["mesh"] = mesh_generator->toYamlNode();
    }
    nd["domain"] = domain.toYamlNode();
    YAML::Node hp;
    for(const Hole<cdtPoint,Vertex_handle>& h: holes)
    {
        hp.push_back(h.toYamlNode());
    }
    nd["holes"] = hp;

    YAML::Node ic;
    for(const InitialConditions<cdtPoint>& icnode: ic_list)
    {
        ic.push_back(icnode.toYamlNode());
    }
    nd["initial conditions"] = ic;
    return nd;
}

/////////////////////////////////////////////////////////////
bool Project::readInput(std::string fname)
{
    YAML::Node doc = YAML::LoadFile(fname);
    fromYamlNode(doc);
    //! \todo validate input!!!!
    return true;
}

/////////////////////////////////////////////////////////////
void Project::generateMesh()
{
    applyInterpolations();
    createDomain();
    createHoles();
    createMesh();
    numberFacesAndGenerateMapOfPoints();
}

/////////////////////////////////////////////////////////////
void  Project::generateOutput(std::string fname)
{
    write2vtk(filePathBuilder::resultsPath() + fname+".vtu");
    write2su2(filePathBuilder::resultsPath() + fname+".su2");
    if(ic_list.size()>0)
    {
        write2ic(fname+"_00000.dat");
    }
}

/////////////////////////////////////////////////////////////
void Project::createDomain()
{
    create_closed_domain(domain.getPoly());
}

/////////////////////////////////////////////////////////////
void Project::createHoles()
{
    for(auto& hole : holes)
    {
        std::vector<cdtPoint> hole_poly = hole.getPoly();
        for(const auto& pt : hole.poly)
        {
            hole_poly.push_back(cdtPoint(pt.x(), pt.y()));
        }
        create_closed_hole(hole, hole_poly, cdtPoint(hole.seed.x(), hole.seed.y()), list_of_seeds);
    }
}

/////////////////////////////////////////////////////////////
void Project::applyInterpolations()
{
    domain.applyInterpolations();
    for(auto& hole : holes)
    {
        hole.applyInterpolations();
    }
}

/////////////////////////////////////////////////////////////
void Project::createMesh()
{
    mesh_generator->generateMesh(list_of_seeds.begin(), list_of_seeds.end());

    /*
    typedef CDT::Point PT;
    typedef CDT::Segment Segment;
    typedef K::Point_2 Point_2;
    typedef K::Segment_2 Segment_2;

    typedef CGAL::Triangulation_data_structure_2<Vb, Fb> Tds;
    typedef CGAL::Constrained_Delaunay_triangulation_2<K, Tds,CGAL::Exact_predicates_tag> Tr;
    typedef CGAL::Delaunay_mesh_area_criteria_2<Tr> crit;


    Segment s(PT(30,-5), PT(40,5));
    std::vector<cdtPoint> square01;
    square01.push_back(cdtPoint(20,10));
    square01.push_back(cdtPoint(-10,10));
    square01.push_back(cdtPoint(-10,5));
    square01.push_back(cdtPoint(20,5));



    cdt.insert_constraint(Point_2(20,10),Point_2(-10,10));
    cdt.insert_constraint(Point_2(-10,10),Point_2(-10,5));
    cdt.insert_constraint(Point_2(-10,5),Point_2(20,5));
    cdt.insert_constraint(Point_2(20,5),Point_2(20,10));


    crit c;
    const double ab = 0.01;
    c.set_area_bound(ab);
    CGAL::refine_Delaunay_mesh_2(cdt,list_of_seeds.begin(),list_of_seeds.end(), c);

    CGAL::refine_Delaunay_mesh_2(cdt,c,true);



    typedef CGAL::Delaunay_mesh_local_size_criteria_2<CDT> CritLocal;
    typedef CGAL::Delaunay_mesh_local_size_criteria_2<CDT> LocalCriteria;



    CGAL::refine_Delaunay_mesh_2(cdt, list_of_seeds.begin(), list_of_seeds.end(), LocalCriteria(0.125,0.2,true, CGAL::Segment_2< K >(square01[0], square01[1])));
    CGAL::refine_Delaunay_mesh_2(cdt, list_of_seeds.begin(), list_of_seeds.end(), LocalCriteria(0.125,0.2,true, CGAL::Segment_2< K >(square01[1], square01[2])));
    CGAL::refine_Delaunay_mesh_2(cdt, list_of_seeds.begin(), list_of_seeds.end(), LocalCriteria(0.125,0.2,true, CGAL::Segment_2< K >(square01[2], square01[3])));
    CGAL::refine_Delaunay_mesh_2(cdt, list_of_seeds.begin(), list_of_seeds.end(), LocalCriteria(0.125,0.2,true, CGAL::Segment_2< K >(square01[3], square01[0])));
    */
}

/////////////////////////////////////////////////////////////
void Project::numberFacesAndGenerateMapOfPoints()
{
    // count triangles that are in domain
    mesh_faces_counter = 0;
    for(CDT::Finite_faces_iterator fit = cdt.finite_faces_begin();
            fit != cdt.finite_faces_end(); ++fit)
    {
        if(fit->is_in_domain())
        {
            ++mesh_faces_counter;
        }
    }
    // generate map of points and corresponding point ids
    int i=0;
    for (CDT::Finite_vertices_iterator it=cdt.finite_vertices_begin();
            it != cdt.finite_vertices_end(); ++it)
    {
        V[it->point()] = i;
        ++i;
    }

    for (Triangulation::Subconstraint_iterator scit = cdt.subconstraints_begin();
            scit != cdt.subconstraints_end();
            ++scit)
    {

        Triangulation::Context _c = cdt.context(scit->first.first,scit->first.second);
        fillBoundaryIds(_c,scit->first.first,scit->first.second);
    }
}

/////////////////////////////////////////////////////////////
void Project::create_closed_domain(const std::vector<cdtPoint> &points)
{
    int pt_idx = 0;
    for(const auto& pt : points)
    {
        Vertex_handle h = cdt.push_back(pt);
        domain.initial_boundary_vertices.insert(std::make_pair(pt_idx,h));
        domain.vertices_ids.insert(std::make_pair(h, pt_idx));
        ++pt_idx;
    }
    for(size_t i = 0; i<domain.initial_boundary_vertices.size()-1; ++i)
    {
        cdt.insert_constraint(domain.initial_boundary_vertices.at(i), domain.initial_boundary_vertices.at(i+1));
    }
    cdt.insert_constraint(domain.initial_boundary_vertices.at(domain.initial_boundary_vertices.size()-1),
                          domain.initial_boundary_vertices.at(0));
}

/////////////////////////////////////////////////////////////
void Project::create_closed_hole(Hole<cdtPoint,Vertex_handle>& hole, const std::vector<cdtPoint> &points,
                                 const cdtPoint &seed, std::list<cdtPoint>& seeds)
{
    int pt_idx = 0;
    for(const auto& pt : points)
    {
        Vertex_handle h = cdt.push_back(pt);
        hole.initial_boundary_vertices.insert(std::make_pair(pt_idx,h));
        hole.vertices_ids.insert(std::make_pair(h, pt_idx));
        ++pt_idx;
    }
    for(size_t i = 0; i<hole.initial_boundary_vertices.size()-1; ++i)
    {
        cdt.insert_constraint(hole.initial_boundary_vertices.at(i), hole.initial_boundary_vertices.at(i+1));
    }
    cdt.insert_constraint(hole.initial_boundary_vertices.at(hole.initial_boundary_vertices.size()-1),
                          hole.initial_boundary_vertices.at(0));
    seeds.push_back(seed);

    /*typedef typename std::map<unsigned, Vertex_handle>::const_iterator MapIterator;
    std::cout << "initial_boundary_vertices" << std::endl;
    for(MapIterator it = hole.initial_boundary_vertices.cbegin(); it != hole.initial_boundary_vertices.cend(); ++it){
      std::cout << " " <<  it->first;
    }
    typedef typename std::map<Vertex_handle, unsigned>::const_iterator MapIterator2;
    std::cout << std::endl << "vertices_ids" << std::endl;
    for(MapIterator2 it = hole.vertices_ids.cbegin(); it != hole.vertices_ids.cend(); ++it){
      std::cout << " " <<  it->second;
    }*/
}

/////////////////////////////////////////////////////////////
void Project::write2vtk(std::string fname)
{
    std::ofstream vtk_file(fname);
    // header
    vtk_file << "<VTKFile type=\"UnstructuredGrid\" ";
    vtk_file << "version=\"0.1\" ";
    vtk_file << "byte_order=\"BigEndian\">" << std::endl;

    int indent_size = 2;
    std::string indent_unit(indent_size, ' ');
    std::string indent = indent_unit;
    vtk_file << indent + "<UnstructuredGrid>" << std::endl;

    int num_vertices = cdt.number_of_vertices();
    int num_cells = mesh_faces_counter;

    indent += indent_unit;
    vtk_file << indent + "<Piece NumberOfPoints=\"" << num_vertices << "\" ";
    vtk_file << "NumberOfCells=\"" << num_cells << "\">" << std::endl;

    // Write vertices
    indent += indent_unit;
    vtk_file << indent + "<Points>" << std::endl;

    indent += indent_unit;
    vtk_file << indent;
    vtk_file << "<DataArray type=\"Float32\" NumberOfComponents=\"3\" Format=\"ascii\">" << std::endl;

    indent += indent_unit;
    for (CDT::Finite_vertices_iterator it=cdt.finite_vertices_begin();
            it != cdt.finite_vertices_end(); ++it)
    {
        vtk_file << indent;
        vtk_file << it->point().x() << " " << it->point().y() << " " << 0 << std::endl;
    }
    indent.erase(indent.length()-indent_size, indent_size);
    vtk_file << indent + "</DataArray>" << std::endl;

    indent.erase(indent.length()-indent_size, indent_size);
    vtk_file << indent + "</Points>" << std::endl;

    // Write triangles
    vtk_file << indent << "<Cells>" << std::endl;
    indent += indent_unit;
    vtk_file << indent;
    vtk_file << "<DataArray type=\"Int32\" Name=\"connectivity\" Format=\"ascii\">";
    vtk_file << std::endl;
    indent += indent_unit;

    for(CDT::Finite_faces_iterator fit = cdt.finite_faces_begin();
            fit != cdt.finite_faces_end(); ++fit)
    {
        if(fit->is_in_domain())
        {
            vtk_file << indent;
            vtk_file << V[(cdt.triangle(fit))[0]] << " ";
            vtk_file << V[(cdt.triangle(fit))[1]] << " ";
            vtk_file << V[(cdt.triangle(fit))[2]] << " " << std::endl;
        }
    }
    indent.erase(indent.length()-indent_size, indent_size);
    vtk_file << indent + "</DataArray>" << std::endl;

    // offsets
    // every element is a three node triangle so all offsets are multiples of 3
    vtk_file << indent;
    vtk_file << "<DataArray type=\"Int32\" Name=\"offsets\" Format=\"ascii\">";
    vtk_file << std::endl;
    int i = 3;
    indent += indent_unit;
    for (int j = 0; j < num_cells; ++j)
    {
        vtk_file << indent << i << std::endl;
        i += 3;
    }
    indent.erase(indent.length()-indent_size, indent_size);
    vtk_file << indent + "</DataArray>" << std::endl;


    // cell types (type 5 is a three node triangle)
    vtk_file << indent;
    vtk_file << "<DataArray type=\"Int32\" Name=\"types\" Format=\"ascii\">";
    vtk_file << std::endl;
    indent += indent_unit;
    for (int j = 0; j < num_cells; ++j)
    {
        vtk_file << indent << "5" << std::endl;
    }
    indent.erase(indent.length()-indent_size, indent_size);
    vtk_file << indent + "</DataArray>" << std::endl;

    indent.erase(indent.length()-indent_size, indent_size);
    vtk_file << indent + "</Cells>" << std::endl;
    // printing out the conservatives - for now only rho...
    if(ic_list.size()>0)
    {
        vtk_file << indent << "<PointData>" << std::endl;
        indent += indent_unit;
        vtk_file << indent;
        // printing rho
        vtk_file << "<DataArray type=\"Float32\" Name=\"rho\" Format=\"ascii\">";
        vtk_file << std::endl;
        indent += indent_unit;
        for (CDT::Finite_vertices_iterator it=cdt.finite_vertices_begin();
                it != cdt.finite_vertices_end(); ++it)
        {
            std::tuple<double, double, double, double> conservatives;
            conservatives = ic_list[ic_in_range(it->point())].getConservative();
            vtk_file << indent << std::get<0>(conservatives) << std::endl;
        }
        indent.erase(indent.length()-indent_size, indent_size);
        vtk_file << indent + "</DataArray>" << std::endl;

        // printing rho_u
        vtk_file << "<DataArray type=\"Float32\" Name=\"rho_u\" Format=\"ascii\">";
        vtk_file << std::endl;
        indent += indent_unit;
        for (CDT::Finite_vertices_iterator it=cdt.finite_vertices_begin();
                it != cdt.finite_vertices_end(); ++it)
        {
            std::tuple<double, double, double, double> conservatives;
            conservatives = ic_list[ic_in_range(it->point())].getConservative();
            vtk_file << indent << std::get<1>(conservatives) << std::endl;
        }
        indent.erase(indent.length()-indent_size, indent_size);
        vtk_file << indent + "</DataArray>" << std::endl;

        // printing rho_v
        vtk_file << "<DataArray type=\"Float32\" Name=\"rho_v\" Format=\"ascii\">";
        vtk_file << std::endl;
        indent += indent_unit;
        for (CDT::Finite_vertices_iterator it=cdt.finite_vertices_begin();
                it != cdt.finite_vertices_end(); ++it)
        {
            std::tuple<double, double, double, double> conservatives;
            conservatives = ic_list[ic_in_range(it->point())].getConservative();
            vtk_file << indent << std::get<2>(conservatives) << std::endl;
        }
        indent.erase(indent.length()-indent_size, indent_size);
        vtk_file << indent + "</DataArray>" << std::endl;

        // printing rho_E
        vtk_file << "<DataArray type=\"Float32\" Name=\"rho_E\" Format=\"ascii\">";
        vtk_file << std::endl;
        indent += indent_unit;
        for (CDT::Finite_vertices_iterator it=cdt.finite_vertices_begin();
                it != cdt.finite_vertices_end(); ++it)
        {
            std::tuple<double, double, double, double> conservatives;
            conservatives = ic_list[ic_in_range(it->point())].getConservative();
            vtk_file << indent << std::get<3>(conservatives) << std::endl;
        }
        indent.erase(indent.length()-indent_size, indent_size);
        vtk_file << indent + "</DataArray>" << std::endl;

        indent.erase(indent.length()-indent_size, indent_size);
        vtk_file << indent + "</PointData>" << std::endl;
    }

    indent.erase(indent.length()-indent_size, indent_size);
    vtk_file << indent + "</Piece>" << std::endl;

    indent.erase(indent.length()-indent_size, indent_size);
    vtk_file << indent + "</UnstructuredGrid>" << std::endl;

    indent.erase(indent.length()-indent_size, indent_size);
    vtk_file << "</VTKFile>" << std::endl;
    vtk_file.close();
}

/////////////////////////////////////////////////////////////
void Project::write2su2(std::string fname)
{
    std::ofstream su2_file(fname);
    su2_file << "%\n";
    su2_file << "% Problem dimension\n";
    su2_file << "%\n";
    su2_file << "NDIME=2\n";

    su2_file << "%\n";
    su2_file << "% Inner elements\n";
    su2_file << "%\n";

    su2_file << "NELEM="<<mesh_faces_counter<<"\n";

    int face_increment=0;
    for(CDT::Finite_faces_iterator fit = cdt.finite_faces_begin();
            fit != cdt.finite_faces_end(); ++fit)
    {
        if(fit->is_in_domain())
        {
            su2_file << "5"<< " ";
            su2_file << V[(cdt.triangle(fit))[0]] << " ";
            su2_file << V[(cdt.triangle(fit))[1]] << " ";
            su2_file << V[(cdt.triangle(fit))[2]] << " ";
            su2_file << face_increment<<std::endl;
            ++face_increment;
        }
    }

    su2_file << "%\n";
    su2_file<< "NPOIN="<<  cdt.number_of_vertices()<<"\n";
    int vert_idx=0;
    for (CDT::Finite_vertices_iterator it=cdt.finite_vertices_begin();
            it != cdt.finite_vertices_end(); ++it)
    {
        su2_file << it->point().x() << " " << it->point().y() << " " << vert_idx << std::endl;
        ++vert_idx;
    }

    su2_file << "%\n";
    su2_file << "% Boundary elements\n";
    su2_file <<"%\n" ;
    su2_file <<"NMARK="<< numberOfBoundaries()<<"\n";
    for(const auto& bdry : domain.getBoundaries())
    {
        su2_file << "MARKER_TAG= "<<bdry.name<<"\n";
        su2_file << "MARKER_ELEMS=" << bdry.mesh_boundary_ids.size() <<"\n";
        for(const auto &idx : bdry.mesh_boundary_ids)
        {
            su2_file << "3\t" << std::get<0>(idx) << "\t" << std::get<1>(idx) << "\n";
        }
    }
    for(const auto& hole : holes)
    {
        for(const auto& bdry : hole.boundaries)
        {
            su2_file << "MARKER_TAG= "<<bdry.name<<"\n";
            su2_file << "MARKER_ELEMS=" << bdry.mesh_boundary_ids.size() <<"\n";
            for(const auto &idx : bdry.mesh_boundary_ids)
            {
                su2_file << "3\t" << std::get<0>(idx) << "\t" << std::get<1>(idx) << "\n";
            }
        }

    }
    su2_file.close();
}

/////////////////////////////////////////////////////////////
void Project::write2ic(std::string fname)
{
    std::ofstream ic_file(fname);
    ic_file<< "\"PointID\", \"x\", \"y\", \"Conservative_1\", \"Conservative_2\", \"Conservative_3\", \"Conservative_4\"\n";
    int vert_idx=0;
    for (CDT::Finite_vertices_iterator it=cdt.finite_vertices_begin();
            it != cdt.finite_vertices_end(); ++it)
    {
        ic_file << vert_idx << "\t"<<it->point().x() << "\t" << it->point().y() << "\t";
        std::tuple<double, double, double, double> conservatives;
        conservatives = ic_list[ic_in_range(it->point())].getConservative();
        ic_file<<std::get<0>(conservatives)<<"\t"<<std::get<1>(conservatives)<<
               "\t"<<std::get<2>(conservatives)<<"\t"<<std::get<3>(conservatives);
        ic_file<< std::endl;
        ++vert_idx;
    }
    ic_file.close();
}

///////////////////////////////////////////////////
void Project::fillBoundaryIds(Triangulation::Context& c,Vertex_handle va, Vertex_handle vb)
{
    Triangulation::Vertices_in_constraint vc_first = c.vertices_begin();
    Triangulation::Vertices_in_constraint vc_last = c.vertices_end();
    --vc_last;
    domain.insertSubConstraint((*vc_first), (*vc_last),V[(va)->point()],V[(vb)->point()]);
    for(auto& hole : holes)
    {
        hole.insertSubConstraint((*vc_first), (*vc_last),V[(va)->point()],V[(vb)->point()]);
    }
}

///////////////////////////////////////////////////
void Project::write_example(std::string fname)
{
    YAML::Node node;
    node["meshDimension"] = 2;
    node["mesh"]["type"] = "Lipschitz";
    node["mesh"]["k"] = 0.1;
    node["mesh"]["TriangleAngle"] = 0.125;
    /*node["mesh"]["type"] = "Simple";
    node["mesh"]["TriangleSize"] = 2;
    node["mesh"]["TriangleAngle"] = 0.125;*/
    // domena
    std::vector<cdtPoint> domain_points =
    {cdtPoint(40,10), cdtPoint(0,10), cdtPoint(-20,10), cdtPoint(-20,-10), cdtPoint(40,-10)};


    YAML::Node poly;
    for(const auto& pt: domain_points)
    {
        YAML::Node ypt;
        ypt[0] = pt.x();
        ypt[1] = pt.y();
        poly.push_back(ypt);
    }
    node["domain"]["name"] = "name";
    node["domain"]["points"] = poly;

    //su2mg::test za_points;
    //YAML::Node tocke;
    //tocke = za_points.ubaci_tocke_u_yaml();

    /*
     * Ovdje ide kod koji ce ubaciti points
     * povezati se sa svojom klasom
     */

    YAML::Node hole01;
    {
        YAML::Node wall;
        std::vector<cdtPoint> hole_points =
        {cdtPoint(10,-2), cdtPoint(8,0), cdtPoint(10,2), cdtPoint(12,0)};
        cdtPoint seed(9,0);
        YAML::Node holepoly;
        for(const auto& pt: hole_points)
        {
            YAML::Node ypt;
            ypt[0] = pt.x();
            ypt[1] = pt.y();
            holepoly.push_back(ypt);
        }

        hole01["name"]="square01";
        YAML::Node spt;
        spt["point"][0] = seed.x();
        spt["point"][1] = seed.y();

        hole01["seed"]=spt;
        hole01["points"]=holepoly;
        node["holes"].push_back(hole01);
    }

    // rupa u domeni 2
    YAML::Node hole02;
    {
        YAML::Node wall;
        std::vector<cdtPoint> hole_points =
        {cdtPoint(10,5), cdtPoint(8,7),cdtPoint(6,5),cdtPoint(8,3)};
        cdtPoint seed(8,5);
        YAML::Node holepoly;
        for(const auto& pt: hole_points)
        {
            YAML::Node ypt;
            ypt[0] = pt.x();
            ypt[1] = pt.y();
            holepoly.push_back(ypt);
        }

        hole02["name"]="square02";
        YAML::Node spt;
        spt["point"][0] = seed.x();
        spt["point"][1] = seed.y();

        hole02["seed"]=spt;
        hole02["points"]=holepoly;
        node["holes"].push_back(hole02);
    }

    // boundaries
    YAML::Node top_boundary;
    top_boundary["name"] = "top";
    top_boundary["ids"].push_back(0);
    top_boundary["ids"].push_back(1);
    top_boundary["ids"].push_back(2);
    node["domain"]["boundaries"].push_back(top_boundary);

    YAML::Node left_boundary;
    left_boundary["name"] = "left";
    left_boundary["ids"].push_back(2);
    left_boundary["ids"].push_back(3);
    node["domain"]["boundaries"].push_back(left_boundary);


    YAML::Node bottom_boundary;
    bottom_boundary["name"] = "bottom";
    bottom_boundary["ids"].push_back(3);
    bottom_boundary["ids"].push_back(4);
    node["domain"]["boundaries"].push_back(bottom_boundary);


    YAML::Node right_boundary;
    right_boundary["name"] = "right";
    right_boundary["ids"].push_back(4);
    right_boundary["ids"].push_back(0);
    node["domain"]["boundaries"].push_back(right_boundary);

    YAML::Node hole_boundary;
    hole_boundary["name"] = "hole01";
    hole_boundary["ids"].push_back(0);
    hole_boundary["ids"].push_back(1);
    hole_boundary["ids"].push_back(2);
    hole_boundary["ids"].push_back(3);
    node["holes"][0]["boundaries"].push_back(hole_boundary);

    YAML::Node hole_boundary2;
    hole_boundary2["name"] = "hole02";
    hole_boundary2["ids"].push_back(0);
    hole_boundary2["ids"].push_back(1);
    hole_boundary2["ids"].push_back(2);
    hole_boundary2["ids"].push_back(3);
    node["holes"][1]["boundaries"].push_back(hole_boundary2);

    std::ofstream fout;
    fout.open(fname);
    fout<<node;
    fout.close();
}
///////////////////////////////////////////////////
void Project::write_nozzle_in_res(std::string fname)
{
    YAML::Node node;
    node["meshDimension"] = 2;
    node["mesh"]["type"] = "Lipschitz";
    node["mesh"]["k"] = 0.08;
    node["mesh"]["TriangleAngle"] = 0.125;
    /*node["mesh"]["type"] = "Simple";
    node["mesh"]["TriangleSize"] = 2;
    node["mesh"]["TriangleAngle"] = 0.125;*/
    // domena


    double x_left = 0.;
    double x_right = 3;
    int n = 40;
    double dx = (x_right-x_left)/(n-1);
    std::valarray<double> x_arr(n);
    for(int i=0; i<n; ++i)
    {
        x_arr[i] = (dx*i);
    }
    std::valarray<double> a_arr( 1+2.2*(x_arr-1.5)*(x_arr-1.5));

    std::valarray<double> r_arr( 0.5*std::sqrt(4.*a_arr/3.14159));
    std::valarray<double> y_up(r_arr);
    std::valarray<double> y_down(-r_arr);


    std::vector<cdtPoint> domain_points;
    for(int i=0; i<(int)x_arr.size(); ++i)
    {
        domain_points.push_back(cdtPoint(x_arr[i],y_up[i]));
    }
    domain_points.push_back(cdtPoint(3,10));
    domain_points.push_back(cdtPoint(40,10));
    domain_points.push_back(cdtPoint(40,-10));
    domain_points.push_back(cdtPoint(3,-10));
    for(int i=(int)x_arr.size()-1; i>=0; --i)
    {
        domain_points.push_back(cdtPoint(x_arr[i],y_down[i]));
    }

    YAML::Node poly;
    for(const auto& pt: domain_points)
    {
        YAML::Node ypt;
        ypt[0] = pt.x();
        ypt[1] = pt.y();
        poly.push_back(ypt);
    }
    node["domain"]["name"] = "name";
    node["domain"]["points"] = poly;

    // boundaries
    YAML::Node inlet_boundary;
    inlet_boundary["name"] = "inlet";
    inlet_boundary["ids"].push_back(0);
    inlet_boundary["ids"].push_back(poly.size()-1);
    node["domain"]["boundaries"].push_back(inlet_boundary);


    YAML::Node top_nozzle_boundary;
    top_nozzle_boundary["name"] = "top_nozzle";
    for(int i=0; i<(int)x_arr.size(); ++i)
    {
        top_nozzle_boundary["ids"].push_back(i);
    }
    node["domain"]["boundaries"].push_back(top_nozzle_boundary);


    YAML::Node top_left_boundary;
    top_left_boundary["name"] = "top_left_boundary";
    top_left_boundary["ids"].push_back(x_arr.size()-1);
    top_left_boundary["ids"].push_back(x_arr.size());
    node["domain"]["boundaries"].push_back(top_left_boundary);
    /*
        YAML::Node top_boundary;
        top_boundary["name"] = "top";
        top_boundary["ids"].push_back(x_arr.size());
        top_boundary["ids"].push_back(x_arr.size()+1);
        node["domain"]["boundaries"].push_back(top_boundary);

        YAML::Node outlet_boundary;
        outlet_boundary["name"] = "outlet";
        outlet_boundary["ids"].push_back(x_arr.size()+1);
        outlet_boundary["ids"].push_back(x_arr.size()+2);
        node["domain"]["boundaries"].push_back(outlet_boundary);


        YAML::Node bottom_boundary;
        bottom_boundary["name"] = "bottom";
        bottom_boundary["ids"].push_back(x_arr.size()+2);
        bottom_boundary["ids"].push_back(x_arr.size()+3);
        node["domain"]["boundaries"].push_back(bottom_boundary);
    */
    YAML::Node bottom_left_boundary;
    bottom_left_boundary["name"] = "bottom_left_boundary";
    bottom_left_boundary["ids"].push_back(x_arr.size()+3);
    bottom_left_boundary["ids"].push_back(x_arr.size()+4);
    node["domain"]["boundaries"].push_back(bottom_left_boundary);

    YAML::Node outlet;
    outlet["name"] = "out";
    outlet["ids"].push_back(x_arr.size());
    outlet["ids"].push_back(x_arr.size()+1);
    outlet["ids"].push_back(x_arr.size()+2);
    outlet["ids"].push_back(x_arr.size()+3);
    node["domain"]["boundaries"].push_back(outlet);

    YAML::Node bottom_nozzle_boundary;
    bottom_nozzle_boundary["name"] = "bottom_nozzle";
    for(int i=(int)x_arr.size()+4; i<(int)domain_points.size(); ++i)
    {
        bottom_nozzle_boundary["ids"].push_back(i);
    }
    node["domain"]["boundaries"].push_back(bottom_nozzle_boundary);

    YAML::Node gamma;
    gamma["gamma"] = 1.4;
    //node["initial conditions"].push_back(gamma);
    YAML::Node ic_01;
    ic_01["name"] = "left region";
    ic_01["x_range"].push_back(-1);
    ic_01["x_range"].push_back(21);
    ic_01["y_range"].push_back(-11);
    ic_01["y_range"].push_back(11);
    ic_01["rho"] = 1.;
    ic_01["u"] = 0.;
    ic_01["v"] = 0.;
    ic_01["p"] = 190000.;
    node["initial conditions"].push_back(ic_01);

    std::ofstream fout;
    fout.open(fname);
    fout<<node;
    fout.close();

}

void Project::write_nozzle_only(std::string fname)
{
    YAML::Node node;
    node["meshDimension"] = 2;
    node["mesh"]["type"] = "Lipschitz";
    node["mesh"]["k"] = 0.05;
    node["mesh"]["TriangleAngle"] = 0.125;

    // domena
    double x_left = 0.;
    double x_right = 3;
    int n = 40;
    double dx = (x_right-x_left)/(n-1);
    std::valarray<double> x_arr(n);
    for(int i=0; i<n; ++i)
    {
        x_arr[i] = (dx*i);
    }
    std::valarray<double> a_arr( 1+2.2*(x_arr-1.5)*(x_arr-1.5));

    std::valarray<double> r_arr( 0.5*std::sqrt(4.*a_arr/3.14159));
    std::valarray<double> y_up(r_arr);
    std::valarray<double> y_down(-r_arr);


    std::vector<cdtPoint> domain_points;
    for(int i=0; i<(int)x_arr.size(); ++i)
    {
        domain_points.push_back(cdtPoint(x_arr[i],y_up[i]));
    }
    for(int i=(int)x_arr.size()-1; i>=0; --i)
    {
        domain_points.push_back(cdtPoint(x_arr[i],y_down[i]));
    }

    YAML::Node poly;
    for(const auto& pt: domain_points)
    {
        YAML::Node ypt;
        ypt[0] = pt.x();
        ypt[1] = pt.y();
        poly.push_back(ypt);
    }
    node["domain"]["name"] = "name";
    node["domain"]["points"] = poly;

    // boundaries
    YAML::Node inlet_boundary;
    inlet_boundary["name"] = "inlet";
    inlet_boundary["ids"].push_back(0);
    inlet_boundary["ids"].push_back(poly.size()-1);
    node["domain"]["boundaries"].push_back(inlet_boundary);


    YAML::Node top_nozzle_boundary;
    top_nozzle_boundary["name"] = "top_nozzle";
    for(int i=0; i<(int)x_arr.size(); ++i)
    {
        top_nozzle_boundary["ids"].push_back(i);
    }
    node["domain"]["boundaries"].push_back(top_nozzle_boundary);


    YAML::Node outlet_boundary;
    outlet_boundary["name"] = "outlet";
    outlet_boundary["ids"].push_back(x_arr.size()-1);
    outlet_boundary["ids"].push_back(x_arr.size());
    node["domain"]["boundaries"].push_back(outlet_boundary);

    YAML::Node bottom_nozzle_boundary;
    bottom_nozzle_boundary["name"] = "bottom_nozzle";
    for(int i=(int)x_arr.size(); i<(int)domain_points.size(); ++i)
    {
        bottom_nozzle_boundary["ids"].push_back(i);
    }
    node["domain"]["boundaries"].push_back(bottom_nozzle_boundary);

    std::ofstream fout;
    fout.open(fname);
    fout<<node;
    fout.close();

}
void Project::write_sod_unsteady(std::string fname)
{
    YAML::Node node;
    node["meshDimension"] = 2;
    node["mesh"]["type"] = "Simple";
    node["mesh"]["TriangleSize"] = 0.5;
    node["mesh"]["TriangleAngle"] = 0.125;
    std::vector<cdtPoint> domain_points =
    {cdtPoint(3.,1.), cdtPoint(0.,1.), cdtPoint(0.,0.), cdtPoint(3.,0.)};

    YAML::Node poly;
    for(const auto& pt: domain_points)
    {
        YAML::Node ypt;
        ypt[0] = pt.x();
        ypt[1] = pt.y();
        poly.push_back(ypt);
    }
    node["domain"]["name"] = "name";
    node["domain"]["points"] = poly;

    YAML::Node top_boundary;
    top_boundary["name"] = "top";
    top_boundary["ids"].push_back(0);
    top_boundary["ids"].push_back(1);
    node["domain"]["boundaries"].push_back(top_boundary);

    YAML::Node left_boundary;
    left_boundary["name"] = "left";
    left_boundary["ids"].push_back(1);
    left_boundary["ids"].push_back(2);
    node["domain"]["boundaries"].push_back(left_boundary);


    YAML::Node bottom_boundary;
    bottom_boundary["name"] = "bottom";
    bottom_boundary["ids"].push_back(2);
    bottom_boundary["ids"].push_back(3);
    node["domain"]["boundaries"].push_back(bottom_boundary);


    YAML::Node right_boundary;
    right_boundary["name"] = "right";
    right_boundary["ids"].push_back(3);
    right_boundary["ids"].push_back(0);
    node["domain"]["boundaries"].push_back(right_boundary);

    YAML::Node gamma;
    gamma["gamma"] = 1.4;
    //node["initial conditions"].push_back(gamma);
    YAML::Node ic_01;
    ic_01["name"] = "left region";
    std::vector<cdtPoint> lr_pts =
    {   cdtPoint(-1,-1), cdtPoint(1,-1), cdtPoint(1,4), cdtPoint(-1,4)   };

    YAML::Node lpoly;
    for(const auto& pt: lr_pts)
    {
        YAML::Node ypt;
        ypt[0] = pt.x();
        ypt[1] = pt.y();
        lpoly.push_back(ypt);
    }
    ic_01["poly"] = lpoly;
    ic_01["rho"] = 1.;
    ic_01["u"] = 0.;
    ic_01["v"] = 0.;
    ic_01["p"] = 100000.;
    node["initial conditions"].push_back(ic_01);

    YAML::Node ic_02;
    ic_02["name"] = "right region";
    std::vector<cdtPoint> rr_pts =
    {   cdtPoint(1,-1), cdtPoint(4,-1), cdtPoint(4,4), cdtPoint(1,4)   };

    YAML::Node rpoly;
    for(const auto& pt: rr_pts)
    {
        YAML::Node ypt;
        ypt[0] = pt.x();
        ypt[1] = pt.y();
        rpoly.push_back(ypt);
    }
    ic_02["poly"] = rpoly;
    ic_02["rho"] = 0.125;
    ic_02["u"] = 0.;
    ic_02["v"] = 0.;
    ic_02["p"] = 10000.;
    node["initial conditions"].push_back(ic_02);
    std::ofstream fout;
    fout.open(fname);
    fout<<node;
    fout.close();
}

void Project::write_problem_7_8_a_steady(std::string fname)
{
    YAML::Node node;
    node["meshDimension"] = 2;
    node["mesh"]["type"] = "Simple";
    node["mesh"]["TriangleSize"] = 0.5;
    node["mesh"]["TriangleAngle"] = 0.125;

    std::vector<cdtPoint> domain_points =
    {   cdtPoint(40,10), cdtPoint(0,10), cdtPoint(0,0), cdtPoint(20,0),
        cdtPoint(25,std::tan(10.*3.1415926/180.)), cdtPoint(40,std::tan(10.*3.1415926/180.))
    };

    YAML::Node poly;
    for(const auto& pt: domain_points)
    {
        YAML::Node ypt;
        ypt[0] = pt.x();
        ypt[1] = pt.y();
        poly.push_back(ypt);
    }
    node["domain"]["name"] = "name";
    node["domain"]["points"] = poly;

    YAML::Node top_boundary;
    top_boundary["name"] = "top";
    top_boundary["ids"].push_back(0);
    top_boundary["ids"].push_back(1);
    node["domain"]["boundaries"].push_back(top_boundary);

    YAML::Node left_boundary;
    left_boundary["name"] = "left";
    left_boundary["ids"].push_back(1);
    left_boundary["ids"].push_back(2);
    node["domain"]["boundaries"].push_back(left_boundary);


    YAML::Node bottom_boundary;
    bottom_boundary["name"] = "bottom";
    bottom_boundary["ids"].push_back(2);
    bottom_boundary["ids"].push_back(3);
    bottom_boundary["ids"].push_back(4);
    bottom_boundary["ids"].push_back(5);
    node["domain"]["boundaries"].push_back(bottom_boundary);


    YAML::Node right_boundary;
    right_boundary["name"] = "right";
    right_boundary["ids"].push_back(5);
    right_boundary["ids"].push_back(0);
    node["domain"]["boundaries"].push_back(right_boundary);

    std::ofstream fout;
    fout.open(fname);
    fout<<node;
    fout.close();
}
void Project::write_nozzle_in_freestream(std::string fname)
{
    YAML::Node node;
    node["meshDimension"] = 2;
    /*node["mesh"]["type"] = "Simple";
    node["mesh"]["TriangleSize"] = 1;
    node["mesh"]["TriangleAngle"] = 0.125;*/
    node["mesh"]["type"] = "Lipschitz";
    node["mesh"]["k"] = 0.1;
    node["mesh"]["TriangleAngle"] = 0.125;

    std::vector<cdtPoint> domain_points =
    {   cdtPoint(40,40), cdtPoint(-40,40), cdtPoint(-40,-40), cdtPoint(40,-40) };

    YAML::Node poly;
    for(const auto& pt: domain_points)
    {
        YAML::Node ypt;
        ypt[0] = pt.x();
        ypt[1] = pt.y();
        poly.push_back(ypt);
    }
    node["domain"]["name"] = "name";
    node["domain"]["points"] = poly;

    YAML::Node top_boundary;
    top_boundary["name"] = "top";
    top_boundary["ids"].push_back(0);
    top_boundary["ids"].push_back(1);
    node["domain"]["boundaries"].push_back(top_boundary);

    YAML::Node left_boundary;
    left_boundary["name"] = "left";
    left_boundary["ids"].push_back(1);
    left_boundary["ids"].push_back(2);
    node["domain"]["boundaries"].push_back(left_boundary);


    YAML::Node bottom_boundary;
    bottom_boundary["name"] = "bottom";
    bottom_boundary["ids"].push_back(2);
    bottom_boundary["ids"].push_back(3);
    node["domain"]["boundaries"].push_back(bottom_boundary);


    YAML::Node right_boundary;
    right_boundary["name"] = "right";
    right_boundary["ids"].push_back(3);
    right_boundary["ids"].push_back(0);
    node["domain"]["boundaries"].push_back(right_boundary);

    YAML::Node hole01;
    {
        YAML::Node wall;
        std::vector<cdtPoint> hole_points =
        {   cdtPoint(-2,0), cdtPoint(0,-1), cdtPoint(5,0),
            cdtPoint(5,-0.5),cdtPoint(0,-1.5), cdtPoint(-2,-0.5)

        };
        cdtPoint seed(0,-1.25);
        YAML::Node holepoly;
        for(const auto& pt: hole_points)
        {
            YAML::Node ypt;
            ypt[0] = pt.x();
            ypt[1] = pt.y();
            holepoly.push_back(ypt);
        }

        hole01["name"]="square01";
        YAML::Node spt;
        spt["point"][0] = seed.x();
        spt["point"][1] = seed.y();

        hole01["seed"]=spt;
        hole01["points"]=holepoly;
        node["holes"].push_back(hole01);
    }

    YAML::Node hole02;
    {
        YAML::Node wall;
        std::vector<cdtPoint> hole_points =
        {   cdtPoint(-2,-5), cdtPoint(0,-4), cdtPoint(5,-5),
            cdtPoint(5,-4.5),cdtPoint(0,-3.5), cdtPoint(-2,-4.5)
        };
        cdtPoint seed(0,-3.75);
        YAML::Node holepoly;
        for(const auto& pt: hole_points)
        {
            YAML::Node ypt;
            ypt[0] = pt.x();
            ypt[1] = pt.y();
            holepoly.push_back(ypt);
        }

        hole02["name"]="square02";
        YAML::Node spt;
        spt["point"][0] = seed.x();
        spt["point"][1] = seed.y();

        hole02["seed"]=spt;
        hole02["points"]=holepoly;
        node["holes"].push_back(hole02);
    }

    YAML::Node hole_boundary;
    hole_boundary["name"] = "hole01";
    for(int i=0; i<5; ++i)
    {
        hole_boundary["ids"].push_back(i);
    }
    node["holes"][0]["boundaries"].push_back(hole_boundary);

    YAML::Node hole_boundary2;
    hole_boundary2["name"] = "hole02";
    for(int i=0; i<5; ++i)
    {
        hole_boundary2["ids"].push_back(i);
    }
    node["holes"][1]["boundaries"].push_back(hole_boundary2);

    std::ofstream fout;
    fout.open(fname);
    fout<<node;
    fout.close();
}
void Project::write_function_example(std::string fname)
{
    YAML::Node node;
    node["meshDimension"] = 2;
    /*
    node["mesh"]["type"] = "Simple";
    node["mesh"]["TriangleSize"] = 0.5;
    node["mesh"]["TriangleAngle"] = 0.125;*/


    node["mesh"]["type"] = "Lipschitz";
    node["mesh"]["k"] = 1;
    node["mesh"]["TriangleAngle"] = 0.125;


    // domena
    std::vector<cdtPoint> domain_points =
    {
        cdtPoint(40,20), cdtPoint(-20,20), cdtPoint(-20,0), cdtPoint(40,0)
    };

    YAML::Node poly;
    for(const auto& pt: domain_points)
    {
        YAML::Node ypt;
        ypt[0] = pt.x();
        ypt[1] = pt.y();
        poly.push_back(ypt);
    }
    node["domain"]["name"] = "name";
    node["domain"]["points"] = poly;

    // boundaries
    YAML::Node top_boundary;
    top_boundary["name"] = "top";
    top_boundary["ids"].push_back(1);
    top_boundary["ids"].push_back(0);
    node["domain"]["boundaries"].push_back(top_boundary);

    YAML::Node left_boundary;
    left_boundary["name"] = "left";
    left_boundary["ids"].push_back(1);
    left_boundary["ids"].push_back(2);
    node["domain"]["boundaries"].push_back(left_boundary);


    YAML::Node bottom_boundary;
    bottom_boundary["name"] = "bottom";
    bottom_boundary["ids"].push_back(2);
    bottom_boundary["ids"].push_back(3);
    node["domain"]["boundaries"].push_back(bottom_boundary);


    YAML::Node right_boundary;
    right_boundary["name"] = "right";
    right_boundary["ids"].push_back(3);
    right_boundary["ids"].push_back(0);
    node["domain"]["boundaries"].push_back(right_boundary);


    YAML::Node hole;
    {
        YAML::Node hole_0;
        std::vector<cdtPoint> hole_points =
        {
            cdtPoint(16, 7.5),
            //cdtPoint(20,10),
            //cdtPoint(-10,10),
            //cdtPoint(-10,5),
            //cdtPoint(20,5)
        };
        cdtPoint seed(15, 7.5);
        YAML::Node holepoly;
        for(const auto& pt: hole_points)
        {
            YAML::Node ypt;
            ypt[0] = pt.x();
            ypt[1] = pt.y();
            holepoly.push_back(ypt);
        }

        hole["name"]="hole_0";
        YAML::Node spt;
        spt["point"][0] = seed.x();
        spt["point"][1] = seed.y();

        hole["seed"]=spt;
        hole["points"]=holepoly;
        node["holes"].push_back(hole);

        // interpolacija lijevo
        {
            YAML::Node h_interp;
            h_interp["ids"].push_back(0);
            h_interp["ids"].push_back(0);
            h_interp["interpolation type"] = "parametric";
            h_interp["t_range_step"] = "0#1#0.01";
            h_interp["functions"] = "cos(15*_pi*t)+10#sin(2*_pi*t)+10";
            h_interp["x_function"] = "cos(2*_pi*t)+15";
            h_interp["y_function"] = "sin(2*_pi*t)+7.5";
            h_interp["additional_vars"] = "a";
            h_interp["additional_vals"] = "2";
            h_interp["parameter name"] = "t";
            /*
                h_interp["function"] = "sin(y)-10";
            h_interp["interpolation type"] = "explicit";
            h_interp["parameter name"] = "y";
                h_interp["size"] = 50;*/
            hole["interpolations"].push_back(h_interp);
        }

        // boundary gore
        YAML::Node boundaries;
        boundaries["name"] = "boundaries";
        boundaries["ids"].push_back(0);
        boundaries["ids"].push_back(1);
        hole["boundaries"].push_back(boundaries);
    }


    std::ofstream fout;
    fout.open(fname);
    fout<<node;
    fout.close();

}


}



