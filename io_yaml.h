//
// This file is part of su2MeshGenerator <https://bitbucket.org/fantaz/su2-mesh-generator>
// You can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// $URL$
// $Id$
//
//
// Author(s)     : Jerko Skific <skific@riteh.hr>

#ifndef _IO_YAML_H_
#define _IO_YAML_H_
#include <iostream>
#include <fstream>
#include <string>
#include "yaml-cpp/yaml.h"
#include "meshGenerator.h"
#include "boundary.h"
#include "hole.h"
#include "interpolation.h"
#include "domain.h"
#include "initialconditions.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Constrained_triangulation_plus_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Delaunay_mesh_face_base_2.h>
#include <CGAL/Delaunay_mesh_size_criteria_2.h>
#include <CGAL/Delaunay_mesh_local_size_criteria_2.h>
#include <CGAL/Triangulation_conformer_2.h>
#include <CGAL/Triangulation_hierarchy_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_2<K> Vbb;
typedef CGAL::Triangulation_hierarchy_vertex_base_2<Vbb> Vb;
typedef CGAL::Delaunay_mesh_face_base_2<K> Fb;
typedef CGAL::Triangulation_data_structure_2<Vb,Fb> TDS;
typedef CGAL::Exact_predicates_tag Itag;
typedef CGAL::Constrained_Delaunay_triangulation_2<K,TDS,Itag> CDT;
typedef CGAL::Triangulation_hierarchy_2<CDT> CDTH;
typedef CGAL::Constrained_triangulation_plus_2<CDT> Triangulation;
typedef Triangulation::Point cdtPoint;
typedef CGAL::Delaunay_mesh_size_criteria_2<CDT> CritPlus;
typedef CGAL::Delaunay_mesher_2<CDT, CritPlus> MesherPlus;
typedef Triangulation::Vertex_handle Vertex_handle;

namespace su2mg {
class Project
{

public:
    Project() : mesh_generator(nullptr) {}

    //! \brief read input yaml file
    bool readInput(std::string fname);

    //! \brief generates mesh
    void generateMesh();

    //! \brief generate Output
    void generateOutput(std::string fname);

    //! \brief input project from YAML file
    void fromYamlNode(YAML::Node n);

    //! \brief write to YAML file
    YAML::Node toYamlNode() const;

private:
    //! \brief This is the place where the interpolated points get inserted in list
    void applyInterpolations();

    //! \brief generate cgal domain out of \see outer_box
    void createDomain();

    //! \brief generate cgal holes
    void createHoles();

    //! \brief finally creating mesh
    void createMesh();

    //! \brief creating structure for su2 dump
    void numberFacesAndGenerateMapOfPoints();

    //! \brief fill boundary mesh ids
    void fillBoundaryIds(Triangulation::Context& c, Vertex_handle va, Vertex_handle vb);

    //! \brief number of all boundaries
    unsigned numberOfBoundaries() const
    {
        unsigned bnds = domain.numberOfBoundaries();
        for(const auto& hole : holes)
        {
            bnds += hole.boundaries.size();
        }
        return bnds;
    }
private:
    //! \brief mesh dimension - currently only 2D is supported
    int meshDim;
    //! \brief domain
    Domain<cdtPoint,Vertex_handle> domain;

    //! \brief list of holes in domain
    std::vector<Hole<cdtPoint,Vertex_handle>> holes;

    //! \brief ic list
    std::vector<InitialConditions<cdtPoint>> ic_list;

    //! \brief finds in which initialCondition is pt
    size_t ic_in_range(cdtPoint& pt)
    {
        for(size_t i=0; i<ic_list.size(); ++i)
        {   
            if(ic_list[i].is_in_range(pt))
            {
                return i; // the pt is in i-th initial condition
            }
        }
        return ic_list.size()-1; // this shouldn't be allowed to happen!
    }
private:
    //! \brief triangulation instance
    Triangulation cdt;

    //! \brief list of seeds that define holes in domain
    std::list<cdtPoint> list_of_seeds;

    //! \brief triangles in domain
    int mesh_faces_counter;

    //! \brief map of points and corresponding point ids
    std::map<cdtPoint, int> V;

    //! \brief part that actually does the meshing
    std::shared_ptr<MeshGeneratorType<Triangulation>> mesh_generator;

private:
    //! \brief generates domain constraints
    void create_closed_domain(const std::vector<cdtPoint> &points);

    //! \brief generates hole constraints inside domain
    void create_closed_hole(Hole<cdtPoint,Vertex_handle>& hole, const std::vector<cdtPoint> &points,
                            const cdtPoint &seed, std::list<cdtPoint>& seeds);
private:
    //! \brief write vtk unstructureg grid
    void write2vtk(std::string fname);

    //! \brief write su2 file
    void write2su2(std::string fname);

    //! \brief write initial condition file
    void write2ic(std::string fname);
public:
    //! \brief yaml show case
    void write_example(std::string fname);

    //! \brief yaml nozzle in reservoir
    void write_nozzle_in_res(std::string fname);

    //! \brief yaml nozzle
    void write_nozzle_only(std::string fname);

    //! \brief yaml nozzle
    void write_nozzle_in_freestream(std::string fname);

    //! \brief write unsteady sod test
    void write_sod_unsteady(std::string fname);

    //! \brief write unsteady sod test
    void write_problem_7_8_a_steady(std::string fname);

    //! \brief set function a bit diff
    void write_function_example(std::string);

};
}


#endif //_IO_YAML_H_
