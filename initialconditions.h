//
// This file is part of su2MeshGenerator <https://bitbucket.org/fantaz/su2-mesh-generator>
// You can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// $URL$
// $Id$
//
//
// Author(s)     : Jerko Skific <skific@riteh.hr>
#ifndef _INITIAL_CONDITIONS_H_
#define _INITIAL_CONDITIONS_H_
#include <string>
#include <vector>
#include <algorithm>
#include "yaml-cpp/yaml.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

namespace su2mg {
template<typename cdtPoint> class InitialConditions
{
    typedef typename CGAL::Exact_predicates_inexact_constructions_kernel K;
public:
    InitialConditions() : gamma(1.4) {}
    void fromYamlNode(YAML::Node n)
    {
        if(n["name"])
        {
            name = n["name"].as<std::string>();
        }
        if(n["poly"])
        {
            for(const auto &ptnode : n["poly"])
            {
                double x = ptnode[0].as<double>();
                double y = ptnode[1].as<double>();
                poly_area.push_back(cdtPoint(x,y));
            }
        }
        if(n["rho"])
        {
            rho = n["rho"].as<double>();
        }
        if(n["u"])
        {
            u = n["u"].as<double>();
        }
        if(n["v"])
        {
            v = n["v"].as<double>();
        }
        if(n["p"])
        {
            p = n["p"].as<double>();
        }
    }
    YAML::Node toYamlNode() const
    {
        YAML::Node nd;
        nd["name"] = name;
        YAML::Node poly;
        for(const cdtPoint& pt: poly_area)
        {
            poly.push_back(pt.x());
            poly.push_back(pt.y());
        }
        nd["poly"] = poly;
        nd["rho"] = rho;
        nd["u"] = u;
        nd["v"] = v;
        nd["p"] = p;
        return nd;
    }
public:
    std::string getName() const {
        return name;
    }
    bool is_in_range(const cdtPoint &pt) const
    {
        
        if(CGAL::bounded_side_2(poly_area.begin(), 
            poly_area.end()/*poly_area.begin()+poly_area.size()*/,pt, K()) != CGAL::ON_UNBOUNDED_SIDE )
        {return true;}
        return false;
    }
    std::tuple<double, double, double, double> getConservative() const
    {
        return std::make_tuple(rho, rho*u, rho*v, 0.5*rho*(
                                   (u*u+ v*v))+p/(gamma-1.));
    }
private:
    std::string name;
    double rho;
    double u;
    double v;
    double p;
    double gamma; // \todo need to read gamma!
    //! \brief domain point list as defined in yaml file
    std::vector<cdtPoint> poly_area;
};
}
#endif //_INITIAL_CONDITIONS_H_

 
