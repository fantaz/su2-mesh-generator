//
// This file is part of su2MeshGenerator <https://bitbucket.org/fantaz/su2-mesh-generator>
// You can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// $URL$
// $Id$
//
//
// Author(s)     : Jerko Skific <skific@riteh.hr>
#ifndef _LINEAR_SPLINE_H_
#define _LINEAR_SPLINE_H_
#include <string>
#include <vector>
#include <algorithm>
#include "yaml-cpp/yaml.h"

namespace su2mg {
//! \brief class that does linear interpolation
//
template<typename cdtPoint> class linearSpline
{
public:
    //! \brief setting default values
    linearSpline(std::vector<cdtPoint>& points, int _N, double _ratio)
    {
        pts.resize(points.size());
        pts[0] = points[0];
        pts[1] = points[1];

        N = _N;
        ratio = _ratio;

        distx = std::abs(pts[0].x()-pts[1].x());
        disty = std::abs(pts[0].y()-pts[1].y());
        /*std::cout<<"pts[0].x() : "<<pts[0].x()<<
        "\tpts[1].x() : "<< pts[1].x()<< "\tdist x : "<<distx<<std::endl;
        std::cout<<"pts[0].y() : "<<pts[0].y()<<
        "\tpts[1].y() : "<< pts[1].y()<< "\tdist y : "<<disty<<std::endl;
        */
        if(pts[0].x()>pts[1].x()) {
            distx = (-1.)*distx;
        }
        if(pts[0].y()>pts[1].y()) {
            disty = (-1.)*disty;
        }

        interp_type = interp_types[0];

        if(ratio <= 0) {
            std::cout << "Ratio should be >= 0" << std::endl;
        }

    };

    //! \brief get vector containing intepolated points
    std::vector<cdtPoint> getInterpolatedPoints() {
        return interpolated_points;
    }

    //! \brief set interpolation type, if not set default will be onesided scheme
    void setIntepolationType(std::string const& n)
    {
        if( std::find(interp_types.begin(), interp_types.end(), n) == interp_types.end())
        {
            std::cout << "Not valid interp_type, sholud be one of the following: ";
            for (auto st : interp_types) std::cout << st << ' ';
            std::cout << std::endl << "You provided: " << n << std::endl;
        }
        interp_type = n;
    }

    //! \brief fill interpolated_points vector with values
    bool generateInterpolatedPoints()
    {

        if( (interp_type.compare("onesided") == 0 ) || (ratio == 1.) )
        {
            oneSided(N, ratio);
        }
        if(  (interp_type.compare("doublesided") == 0) && (ratio != 1.) )
        {
            doubleSided(N, ratio);
        }
        dx.resize(N+1); // imamo N razmaka
        for(int i=0; i<N; i++)
        {
            dx[i] = std::abs(tcoords[i+1]-tcoords[i]);
        }
        interpolated_points.resize(N+1);
        interpolated_points[0] = pts[0];
        for(int i=1; i< N+1; ++i)
        {
            interpolated_points[i] = cdtPoint(interpolated_points[i-1].x()+distx*dx[i-1],
                                              interpolated_points[i-1].y()+disty*dx[i-1]);
        }
        interpolated_points.erase(interpolated_points.begin());
        /*std::cout<<"Number of points N = "<<N<<std::endl;
        std::cout<<"interpolated_points size = "<<interpolated_points.size()<<std::endl;
        std::cout<<"firstpoint = "<<interpolated_points[0].x()<<":"<<
        interpolated_points[0].y()<<std::endl;
        */
        return true;
    }

private:

    //! \brief distance between points in x and y direction
    double distx, disty;

    //! \brief ratio between last and first interval
    double ratio;

    //! \brief user defined number of points
    int N;

    //! \brief interpolation type, "onesided","doublesided"
    std::string interp_type;

    //! \brief interpolation types
    std::vector<std::string> interp_types = {"onesided","doublesided"};

    //! \brief list of interpolated points
    std::vector<cdtPoint> interpolated_points;
    std::vector<double> tcoords, dx;

    //! \brief vector with first and last point that we interpolate between
    std::vector<cdtPoint> pts;

private:

    //! \brief onesided scheme
    //! we are using ratio to place intepolated points between two original points, the closer the ratio to 0 points will be
    //! placed closer to endpoint, if 1 will be equidistand , if >1 will be grouped towards start point
    void oneSided(int N, double ratio)
    {
        double dist = 1.;
        double c = std::pow(ratio, 1./(N));
        double gs,x_1;

        if(ratio == 1.)
        {
            gs = N+1;
        }
        else
        {
            gs = (1-std::pow(c,N))/(1-c);
        }

        x_1 = dist/gs;

        tcoords.resize(N+2);
        int i(0);
        if( ratio != 1.)
        {
            std::generate(tcoords.begin(), tcoords.begin()+(N+2), [&] {
                return x_1 * (1-std::pow(c,i++)) / (1-c);
            });
        }
        else
        {
            std::generate(tcoords.begin(), tcoords.begin()+(N+2), [&] {
                return x_1*i++;
            });
        }
    };

    //! \brief doublesided scheme
    //! we apply ratio to two segments of the line, line now has center node, if even number of intervals it is center point,
    //! if we have odd number of intervals then it is a point in the middle of the center interval
    void doubleSided(int N, double ratio)
    {
        double dist = 1.;
        double gs, c, x_1;

        if( (N+1)%2 == 1 )
        {
            gs = 0;
            c = std::pow(ratio,1./((N+1)/2));

            for(int i=0; i<(N+1)/2 ; i++) {
                gs = gs + std::pow(c,i);
            }

            gs = gs * 2;
            gs = gs + std::pow(c,(N+1)/2);
        } else
        {
            c = std::pow(ratio, 1./  ( (N+1)/2-1. ) ) ;
            gs = (1-std::pow(c, ((N+1)/2))) / (1-c) ;
        }

        if ((N+1)%2 == 1 )
        {
            x_1 = 1./gs;
        }
        else
        {
            x_1 = dist/2./gs;
        }

        if( (N+1)%2 == 1)
        {
            tcoords.resize((N+2)/2);
            int end(0);
            std::generate(tcoords.begin(), tcoords.begin()+((N+1)/2+1), [&] {
                return (x_1 * (1 - std::pow(c,end++))) / (1-c);
            });
            tcoords.push_back(tcoords[end-1] + ((std::pow(c,(N+1)/2)) * (1./gs)));
            for(int i=(N+1)/2-1; i>-1 ; i--)
            {
                tcoords.push_back(tcoords[end++] + ((std::pow(c,i)) * (1./gs)));
            }
        }
        else
        {
            tcoords.resize((N+1)/2+1);
            int end(0);

            std::generate(tcoords.begin(), tcoords.begin()+((N+1)/2+1), [&] {
                return x_1 * (1 - std::pow(c,end++))/(1-c);
            });
            for(int i=(N+1)/2-1; i>-1 ; i--)
            {
                tcoords.push_back(tcoords[end++-1] + tcoords[i+1] - tcoords[i]);
            }
        }

    }
};
}
#endif //_LINEAR_SPLINE_H_

