//
// This file is part of su2MeshGenerator <https://bitbucket.org/fantaz/su2-mesh-generator>
// You can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// $URL$
// $Id$
//
//
// Author(s)     : Jerko Skific <skific@riteh.hr>
#ifndef _INTERPOLATION_H_
#define _INTERPOLATION_H_
#include <string>
#include <vector>
#include <algorithm>
#include "yaml-cpp/yaml.h"
#include "linearspline.h"
#include "muParser.h"

#include <cmath>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>


namespace su2mg {
//! \brief Interpolation description
template<typename cdtPoint> class Interpolation
{
    typedef typename CGAL::Exact_predicates_inexact_constructions_kernel K;
private:
    //! \brief boundary point ids
    std::vector<unsigned> ptids;

    //! \brief function name
    std::string f_name;

    //! \brief number of points that need to be inserted
    int point_number;

    //! \brief interpolation type
    //! could be one of the following:
    //! linear spline, explicit (iterates over x or y), parametric (need x(t) and y(t))
    std::string interp_type;

    //! \brief parameter that fn iterates over, could be x,y or t
    std::string fn_parameter;

    //! \brief list of interpolated points
    std::vector<cdtPoint> interpolated_points;

    //! \brief last first ratio
    double ratio;

    //! \brief symetric or non-symetric scheme; defined as onesided or doublesided
    std::string scheme;
    
    std::string xFunction;
    std::string yFunction;
    std::string additionalVars;
    std::string additionalVals;
    std::string tRangeStep;
    std::string functions;

public:
    int getPointNumber() const {
        return point_number;
    }
    std::string getFunction() const {
        return f_name;
    }

    std::vector<unsigned> getPtIds() const {
        return ptids;
    }
    unsigned numOfptIds()const {
        return ptids.size();
    }

    std::string getInterpType() const {
        return interp_type;
    }
    std::string getFnParam() const {
        return fn_parameter;
    }

    //! \brief gets list of interpolated points
    std::vector<cdtPoint> getInterpPoints() const {
        return interpolated_points;
    }
    unsigned numberOfInterpolatedPoints() const {
        return interpolated_points.size();
    }

    //! \brief Generates points that will be interpolated and stores them in \var interpolated_points
    void generatePoints(std::vector<cdtPoint>& pts)
    {
        interpolated_points.clear();

        if(interp_type == "explicit")
        {
            interpolated_points.resize(point_number);
            double dist = std::sqrt(CGAL::squared_distance<K>(pts[0], pts[1]));
            double del = dist/(point_number+1.); // kako ubacujem pt_num tocaka, imam pt_num+1 interval
            if(pts[0]>pts[1]) {
                del = (-1.)*del;   // ovdje provjeravam u kojem smjeru ide interp.
            }
            double fVal;
            mu::Parser parser;
            parser.SetExpr(f_name);
            assert(fn_parameter == "x" || fn_parameter == "y");
            if(fn_parameter == "x")
            {
                parser.DefineVar("x", &fVal);
                for(int i=1; i< point_number+1; ++i)
                {
                    fVal= pts[0].x()+i*del;
                    interpolated_points[i-1]=cdtPoint(fVal, parser.Eval());
                }
            }
            else if(fn_parameter == "y")
            {
                parser.DefineVar("y", &fVal);
                for(int i=1; i< point_number+1; ++i)
                {
                    fVal= pts[0].y()+i*del;
                    interpolated_points[i-1]=cdtPoint(parser.Eval(), fVal);
		    //printf("%lf %lf\n",parser.Eval(), fVal);
                }                
            }
        }
        else if(interp_type == "linear spline")
        {
            interpolated_points.resize(point_number);
            linearSpline<cdtPoint> ls(pts,(double) point_number, ratio);
            ls.setIntepolationType(scheme);
            ls.generateInterpolatedPoints();
            interpolated_points = ls.getInterpolatedPoints();
        }
        else if(interp_type == "parametric")
	{
	  
	  std::cout << "" << xFunction << ", " << yFunction 
	  << ", " << tRangeStep << ", " << additionalVars << ", " << additionalVals << std::endl;
	  
	  std::vector<std::string> TRS = parseString(tRangeStep,"#");
	  std::vector<std::string> AVALS = parseString(additionalVals,"#");
	  std::vector<std::string> AVARS = parseString(additionalVars,"#");
	  
	  point_number = ( double(std::atof(TRS[1].c_str())) - double(std::atof(TRS[0].c_str())))
	  /double(std::atof(TRS[2].c_str()));
	  interpolated_points.resize(point_number);
	  double dist = std::sqrt(CGAL::squared_distance<K>(pts[0], pts[1]));
          double del = dist/(point_number+1.); // kako ubacujem pt_num tocaka, imam pt_num+1 interval
          if(pts[0]>pts[1]) {
	    del = (-1.)*del;   // ovdje provjeravam u kojem smjeru ide interp.
	  }
	  double fVal;
	  
	  mu::Parser parser;
	  parser.SetArgSep('#');
	  
	  assert(fn_parameter == "t");
	  parser.DefineConst("_pi", M_PI );
	  parser.DefineVar("t", &fVal);
	  parser.SetExpr(functions);
	  parser.SetDecSep('.');
	  
	  //mu::value_type *r;// tu bi trebali biti rez
	  
	  double start = double(std::atof(TRS[0].c_str()));
	  double end = double(std::atof(TRS[1].c_str()));
	  double step = double(std::atof(TRS[2].c_str()));
	  
	  /*
	  interpolated_points.resize((end-start)/step);
	  fVal = start;
	  for(int i=1; i< point_number+1; ++i)
	  {
	    parser.Eval(r, fVal);
	    std::cout << "iteracija " << i 
	    << " rezultata: " << parser.GetNumResults() 
	    << " fVAl: " << fVal
	    << std::endl;
	    interpolated_points[i-1] = cdtPoint(r[0], r[1]);    
	    fVal += step;
	  }*/
	  
	  
	  interpolated_points.resize((end-start)/step);
	  parser.SetExpr(xFunction);
	  fVal = start;
	  for(int i=1; i< point_number+1; ++i)
	  {
	    interpolated_points[i-1] = cdtPoint(parser.Eval(),0);        
	    fVal += step;
	  }
	  parser.SetExpr(yFunction);
	  fVal=start;
	  for(int i=1; i< point_number+1; ++i)
	  {
	    interpolated_points[i-1] = cdtPoint(interpolated_points[i-1].x(),parser.Eval());	    
	    fVal += step;
	  }
	  std::cout << "START" << std::endl;
	  for(auto& p : interpolated_points)
	  {
	    std::cout << p.x() << " " << p.y() << std::endl; 
	  }
	  
	}
    }

    std::vector<std::string> parseString(std::string toParse, std::string delimiter)
    {
      size_t pos = 0;
      std::vector<std::string> parsed;
      std::string temp;
      
      if( toParse.find(delimiter) == std::string::npos)
      {
	parsed.push_back(toParse);
	std::cout << "Parsed vector" << std::endl;
	for(auto& s : parsed){std::cout << s << " ";}      
	std::cout << std::endl;	
	return parsed;
      }else
      {
	while ((pos = toParse.find(delimiter)) != std::string::npos)
	{
	  temp = toParse.substr(0, pos); 
	  parsed.push_back(temp);
	  toParse.erase(0, pos + delimiter.length());	  
	}
	temp = toParse.substr(0, pos); 
	parsed.push_back(temp);
	toParse.erase(0, pos + delimiter.length());
	std::cout << "Parsed vector" << std::endl;
	for(auto& s : parsed){std::cout << s << " ";}      
	std::cout << std::endl;
	return parsed;
      }
    }

public:
    void fromYamlNode(YAML::Node n)
    {
        if(n["ids"])
        {
            for(const auto idnode : n["ids"])
            {
                ptids.push_back(idnode.as<int>());
            }
        }
        if(n["function"])
        {
            f_name = n["function"].as<std::string>();
        }
        if(n["interpolation type"])
        {
            interp_type = n["interpolation type"].as<std::string>();
        }
        if(n["parameter name"])
        {
            fn_parameter = n["parameter name"].as<std::string>();
        }
        if(n["size"])
        {
            point_number= n["size"].as<int>();
        }
        if(n["ratio"])
        {
            ratio = n["ratio"].as<double>();
        }
        if(n["scheme"])
        {
            scheme = n["scheme"].as<std::string>();
        }
        
        //
        if(n["functions"])
	{
	  functions = n["functions"].as<std::string>();
	}
        if(n["x_function"])
	{
	  xFunction = n["x_function"].as<std::string>();
	}
	if(n["y_function"])
	{
	  yFunction = n["y_function"].as<std::string>();
	}
	if(n["additional_vars"])
	{
	  additionalVars = n["additional_vars"].as<std::string>();
	}
	if(n["additional_vals"])
	{
	  additionalVals = n["additional_vals"].as<std::string>();
	}
	if(n["t_range_step"])
	{
	  tRangeStep = n["t_range_step"].as<std::string>();
	}
        
    }
};
}
#endif //_INTERPOLATION_H_

