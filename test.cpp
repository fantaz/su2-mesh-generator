#include <iostream>
#include "muParser.h"
#include "io_yaml.h"

// Function callback
double MySqr(double a_fVal) 
{ 
  return a_fVal*a_fVal; 
}

// main program
int main(int , char**)
{
  using namespace mu;
  YAML::Node node;
  
  

  try
  {
    double fVal = 2;
    Parser p;
    p.DefineVar("a", &fVal); 
    p.DefineFun("MySqr", MySqr); 
    //p.SetExpr("MySqr(a)*_pi+min(10,a)");
    p.SetExpr("MySqr(a)");
    std::cout << p.Eval() << std::endl;
    
    
    
    for (std::size_t a=0; a<100; ++a)
    {
      fVal = a;  // Change value of variable a
      std::cout << p.Eval() << std::endl;
    }
  }
  catch (Parser::exception_type &e)
  {
    std::cout << e.GetMsg() << std::endl;
  }
  return 0;
}