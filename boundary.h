//
// This file is part of su2MeshGenerator <https://bitbucket.org/fantaz/su2-mesh-generator>
// You can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// $URL$
// $Id$
//
//
// Author(s)     : Jerko Skific <skific@riteh.hr>
#ifndef _BOUNDARY_H_
#define _BOUNDARY_H_
#include <string>
#include <vector>
#include <algorithm>
#include "yaml-cpp/yaml.h"

namespace su2mg {
//! \brief Boundary imported from YAML file
class Boundary
{
public:
    //! \brief boundary name
    std::string name;

    //! \brief boundary point ids
    std::vector<unsigned> ptids;

    //! \brief list of mesh edges that are part of this boundary
    std::vector<std::tuple<unsigned, unsigned>> mesh_boundary_ids;

    //! \brief checks if points with ida and idb are contained in boundary
    //! true if point ids are found, false otherwise
    bool isEdgeOnBounary(unsigned ida, unsigned idb)
    {
        if(std::find(ptids.begin(), ptids.end(), ida)!=ptids.end() &&
                std::find(ptids.begin(), ptids.end(), idb)!=ptids.end())
        {
            return true;
        }
        return false;
    }

    void fromYamlNode(YAML::Node n)
    {
        if(n["name"])
        {
            name = n["name"].as<std::string>();
        }
        if(n["ids"])
        {
            for(const auto idnode : n["ids"])
            {
                ptids.push_back(idnode.as<int>());
            }
        }
    }
    YAML::Node toYamlNode() const
    {
        YAML::Node b;
        b["name"] = name;
        for(const auto& id : ptids)
        {
            b["ids"].push_back(id);
        }
        return b;
    }
};
}
#endif //_BOUNDARY_H_
